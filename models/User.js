const mongoose = require("mongoose") ;

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		require: [true, "First name is required."]
	},
	LastName: {
		type: String,
		require: [true, "Last name is required."]
	},
	mobileNo: {
		type: String,
		require: [true, "Mobile number is required."]
	},
	email: {
		type: String,
		require: [true, "Email is required."]
	},
	password: {
		type: String,
		require: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{		
			courseId: {
				type: String,
				require: [true, "Course ID is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
			,
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})



module.exports = mongoose.model("User", userSchema);