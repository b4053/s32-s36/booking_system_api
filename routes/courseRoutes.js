const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers");
const auth = require("../auth");


// router.post("/", (req, res) => {
// 	CourseController.addCourse(req.body).then(result => res.send(result)
// 	)
// })




// s34 ACTIVITY
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userAdmin = userData.isAdmin;
	if (userAdmin) {
		CourseController.addCourse(req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
});



//RETRIEVING ALL COURSES

router.get("/all", (req, res) => {
	CourseController.getAllCourses(req.body).then(result => res.send(result));
});


//RETRIEVING ALL ACTIVE COURSES

router.get("/allActive", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result));
});


//RETRIEVING A SPECIFIC COURSE

router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	CourseController.getCourse(req.params.courseId).then(result => res.send(result));
});


//UPDATE A COURSE

router.put("/:courseId", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
});


//s35 ACTIVITY
//ARCHIVE A COURSE

router.put("/:courseId/archive", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		CourseController.archiveCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
});




























module.exports = router;