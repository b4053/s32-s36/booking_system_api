const express = require("express");
const router = express.Router();
const auth = require("../auth");

const UserController = require("../controllers/userControllers");




//CHECK DUPLICATE EMAILS

router.post("/checkEmail", (req,res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result))
});


//REGISTRATION FOR USER

router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})


//LOGIN

router.post("/login", (req,res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
})



// ======================================================================================

//s33 ACTIVITY
// // 1. Create a /details route that will accept the user’s Id to retrieve the details of a user.

// router.post("/details", (req,res) => {
// 	UserController.getProfile(req.body).then(result => res.send(result));
// })

// ======================================================================================
 


//TOKEN VERIFICATION

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	UserController.getProfile(userData.id).then(result => res.send(result))
})




//ENROLL USER TO A COURSE

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	UserController.enroll(data).then(result => res.send(result));
})
















module.exports = router;