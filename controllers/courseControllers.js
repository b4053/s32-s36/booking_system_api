const Course = require ("../models/Course");



//CREATE NEW COURSE
// 1. CREATE A NEW COURSE OBJECT
// 2. SAVE TO THE DATABASE
// 3. ERROR HANDLING

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});
	return newCourse.save().then((course, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}


//RETRIEVING ALL COURSES

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}


//RETRIEVING ALL ACTIVE COURSES

module.exports.getAllActive = () => {
	return Course.find({ isActive: true}).then(result => {
		return result;
	})
} 


//RETRIEVE SPECIFIC COURSE

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}


//UPDATE A COURSE
// 1. CREATE A VARIABLE WHICH WILL CONTAIN THE INFO RETRIEVED FROM THE REQUEST BODY 
// 2. FIND AND UPDATE COURSE USING THE COURSE ID

module.exports.updateCourse = (courseId, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}



// s35 ACTIVITY
//ARCHIVE A COURSE

module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams, archivedCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}