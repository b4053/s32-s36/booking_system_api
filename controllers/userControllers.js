const User = require('../models/User');
const Course = require('../models/Course');


// encrypted password
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
}


//BUSINESS LOGIC - USER REGISTRATION
/*
STEPS:
1. CREATE A NEW USER OBJECT 
2. MAKE SURE THAT THE PASSWORD IS ENCRYPTED\
3. SAVE THE NEW USER TO THE DATABASE
*/


module.exports.registerUser = (reqBody) => {
	// create a new user object
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error) => {
		if(error){
			return false;
		} else {
			return true
		}
	})
}


//LOGIN - USER AUTHENTICATION
/*
STEPS:
1. CHECK THE DATABASE IF THE USER EMAIL EXIST
2. COMPARE THE PASSWORD PROVIDED IN THE LOGIN FORM WITH THE PASSWORD STORED IN THE DATABASE
3. GENERATE/RETUEN A JSON WEB TOKEN IF THE USER IS SUCCESSFULLY LOGED IN AND RETURN FALSE IF NOT
*/


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if( result == null){
			return false;
		} else {
			const isPasswordCorrrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrrect){
				return { accessToken : auth.createAccessToken(result.toObject()) }
			} else {
				return false;
			}
		}
	})

}

// s33 ACTIVITY
/*2. Create a getProfile controller method for retrieving the details of the user:
//         a. Find the document in the database using the user's ID
//         b. Reassign the password of the returned document to an empty string
//         c. Return the result back to the frontend
*/


module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody).then((result) => {if( result == null){
			return false;
		} else {
			result.password = "";
			return	result;
			}
		}
	)}


//ENROLL USER TO A COURSE
/* 
1. FIND THE DOCUMENT USING THE USER'S ID
2. ADD THE COURSE ID TO THE USER'S  ENROLLMENT ARRAY USING THE PUSH METHOD.
3. ADD THE USERID TO THE COURSE'S ENROLLEES ARRAYS USING THE PUSH METHOD.
4. SAVE THE DOCUMENT.

ASYNC AND AWAIT -  ALLOW THE PROCESSES TO WAIT FOR EACH OTHER

*/

module.exports.enroll = async (data) => {
	//ADD THE COURSEID TO THE ENROLLMENTS ARRAY OF THE USER
	let isUserUpdated = await User.findById(data.userId).then(user => {
		//PUSH THE COURSE ID TO ENROLLMENTS PROPERTY
		user.enrollments.push({ courseId: data.courseId});
		//SAVE
		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//ADD THE USERID IN THE COURSE'S ENROLLMENTS PROPERTY
		course.enrollees.push({ userId: data.userId});
		return course.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true
			}
		})
	});

	//VALIDATION

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	}
};